import os
import requests
import sys

from collections import deque, namedtuple
from configparser import ConfigParser
from typing import Dict, List, Optional, Tuple, Union

CONFIG_PATH = 'config.ini'
DEFAULT_CONFIG_VALUES = {
    'global': {
        'ip_api_url': 'https://api.ipify.org',
        'dns_api_url': 'https://dns.api.gandi.net/api/v5'
    },
    'local': {
        'dns_api_key': 'UNDEFINED',
        'server_url': 'localhost',
        'ttl': 900
    }
}


LocalAppConfig = namedtuple(
    'LocalAppConfig',
    DEFAULT_CONFIG_VALUES['local'].keys()
)


class AppConfig(object):
    """ Class for parsing and using the config file """
    def __init__(self, config_path: str):
        config_parser = AppConfig.obtain_config(config_path)
        self._ip_api_url = config_parser.get('global', 'ip_api_url')
        self._dns_api_url = config_parser.get('global', 'dns_api_url')
        self._hosts = {}
        for section in config_parser.sections():
            if section == 'global':
                continue
            local_config_options = {}
            for option in config_parser.options(section):
                local_config_options[option] = config_parser.get(
                    section,
                    option
                )
            # By using a namedtuple we ensure the local configs are immutable
            self._hosts[section] = LocalAppConfig(**local_config_options)

    @property
    def ip_api_url(self):
        return self._ip_api_url

    @property
    def dns_api_url(self):
        return self._dns_api_url

    @property
    def hosts(self):
        return self._hosts.items()

    def get_server_config(self, server: str) -> LocalAppConfig:
        return self._hosts[server]

    @staticmethod
    def obtain_config(config_path: str) -> ConfigParser:
        """
        Obtain a config file and populate it with defaults where needed.
        Also writes to the config file with any missing values
        """
        parser = ConfigParser()
        parser.read(config_path)
        parser_has_changed = False
        for section in DEFAULT_CONFIG_VALUES.keys():
            if not parser.has_section(section):
                parser.add_section(section)
                parser_has_changed = True
            for option in DEFAULT_CONFIG_VALUES[section].keys():
                if not parser.has_option(section, option):
                    parser.set(
                        section,
                        option,
                        DEFAULT_CONFIG_VALUES[section][option]
                    )
                    parser_has_changed = True
        if parser_has_changed:
            with open(config_path, 'w') as config_file:
                parser.write(config_file)
        return parser


class GandiDNSRecord(object):
    """ Represents and takes care of a Gandi.net DNS record """
    def __init__(self, *, api_url: str, config: LocalAppConfig):
        if config.dns_api_key == 'UNDEFINED':
            raise EnvironmentError(
                'Please set the DNS api key in your config.ini!'
            )
        self.api_url = api_url
        self.api_key = config.dns_api_key
        self.server_url = config.server_url
        self.ttl = config.ttl
        self._api_works_cache = None
        self._has_api_auth_cache = None
        self._record_url = None

    def update_record_ip(self, ip_address: str, record_type: str = 'A') -> None:
        record_data = self._get_record_data(record_type)
        for value in record_data['rrset_values']:
            if value == ip_address:
                # No need to update!
                print('Not updating DNS: IPs are already equal!')
                return
        record_data['rrset_values'] = [ip_address]
        response = self._api_put(
            full_url=record_data['rrset_href'],
            json=record_data
        )
        try:
            if response['code'] != 200 and response['code'] != 201:
                raise Exception(
                    'Could not update DNS record! Error: {}'.format(
                        response['message']
                    )
                )
        except KeyError:
            pass

    def _api_get(self, **kwargs) -> Union[Dict, List]:
        """ Perform a GET request to the Gandi LiveDNS API """
        self._check_api()
        return self._api_get_unsafe(**kwargs)

    def _api_get_unsafe(
            self,
            endpoint: Optional[str] = None,
            full_url: Optional[str] = None
    ) -> Union[Dict, List]:
        if full_url is not None:
            url = full_url
        else:
            url = self._get_api_url(endpoint)
        response = requests.get(url, headers={'X-Api-Key': self.api_key})
        return response.json()

    def _api_put(self, **kwargs) -> Union[Dict, List]:
        """ Perform a PUT request to the Gandi LiveDNS API """
        self._check_api()
        return self._api_put_unsafe(**kwargs)

    def _api_put_unsafe(
            self,
            endpoint: Optional[str] = None,
            full_url: Optional[str] = None,
            data=None,
            json=None
    ) -> Union[Dict, List]:
        if full_url is not None:
            url = full_url
        else:
            url = self._get_api_url(endpoint)
        response = requests.put(
            url,
            headers={'X-Api-Key': self.api_key},
            data=data,
            json=json
        )
        return response.json()

    def _check_api(self) -> None:
        if not self._api_works():
            raise EnvironmentError(
                'API cannot be reached or is not properly configured!'
            )
        if not self._has_api_auth():
            raise EnvironmentError(
                'You are not authorized to access '
                'protected areas of the LiveDNS API!'
            )

    def _api_works(self) -> bool:
        if self._api_works_cache is not None:
            return self._api_works_cache
        url = self._get_api_url('') + '/'
        response = requests.get(url, headers={'X-Api-Key': self.api_key})
        try:
            result = response.json()
        except ValueError:
            self._api_works_cache = False
            return False
        try:
            _ = result['zone_href']
        except KeyError:
            self._api_works_cache = False
            return False
        self._api_works_cache = True
        return True

    def _has_api_auth(self) -> bool:
        if not self._api_works():
            return False
        if self._has_api_auth_cache is not None:
            return self._has_api_auth_cache
        url = self._get_api_url('zones')
        response = requests.get(url, headers={'X-Api-Key': self.api_key})
        try:
            result = response.json()
        except ValueError:
            self._has_api_auth_cache = False
            return False
        if isinstance(result, list):
            self._has_api_auth_cache = True
            return True
        else:
            self._has_api_auth_cache = False
            return False

    def _get_api_url(self, endpoint: str) -> str:
        """ Get url to be used with Gandi LiveDNS API """
        api_url = self.api_url
        if not api_url.endswith('/'):
            api_url = api_url + '/'
        if endpoint.endswith('/'):
            endpoint = endpoint[:-1]
        return api_url + endpoint

    def _get_zones(self) -> List:
        return self._api_get(endpoint='zones')

    def _get_records_for_zone(self, zone_uuid: str) -> List:
        return self._api_get(endpoint='zones/{}/records'.format(zone_uuid))

    def _find_record_url(self, record_type: str = 'A') -> Optional[str]:
        if self._record_url is not None:
            return self._record_url
        zone_uuid, rrset_name = self._find_zone_uuid_and_rrset_name()
        if zone_uuid is None:
            return None
        records = self._get_records_for_zone(zone_uuid)
        record_url = None
        for record in records:
            try:
                record_rrset_name = record['rrset_name']
                record_rrset_type = record['rrset_type']
            except KeyError:
                continue
            if (record_rrset_name == rrset_name
                    and record_rrset_type == record_type):
                record_url = 'zones/{}/records/{}/A'.format(
                    zone_uuid,
                    rrset_name
                )
        self._record_url = record_url
        return record_url

    def _find_zone_uuid_and_rrset_name(self) \
            -> Tuple[Optional[str], Optional[str]]:
        zones = self._get_zones()
        correct_zone = None
        correct_rrset_name = None
        for zone in zones:
            try:
                zone_name = zone['name']
            except KeyError:
                continue
            rrset_name_parts = deque([])
            url_parts = deque(self.server_url.split('.'))
            while len(url_parts) > 0:
                potential_match = '.'.join(url_parts)
                if potential_match == zone_name:
                    try:
                        correct_zone = zone['uuid']
                    except KeyError:
                        continue
                    correct_rrset_name = '.'.join(rrset_name_parts)
                    break
                rrset_name_parts.append(url_parts.popleft())
            if correct_zone is not None:
                break
        return correct_zone, correct_rrset_name

    def _get_record_data(self, record_type: str = 'A') -> Dict:
        record_url = self._find_record_url(record_type=record_type)
        if record_url is None:
            raise ValueError(
                'No {} record for {} found!'.format(
                    record_type,
                    self.server_url
                )
            )
        return self._api_get(endpoint=record_url)


def get_my_ip(config: AppConfig) -> str:
    """ Get external IP of the server """
    try:
        response = requests.get(config.ip_api_url)
    except requests.HTTPError as e:
        print(
            'Unable to get external IP address: Error {}'.format(
                e.response.status_code
            )
        )
        sys.exit(1)
    return response.text


def main() -> None:
    config_path = CONFIG_PATH
    if not config_path.startswith('/'):
        script_dir = os.path.dirname(os.path.realpath(__file__))
        config_path = os.path.join(script_dir, config_path)
    app_config = AppConfig(config_path)
    print('Getting external IP...')
    my_ip = get_my_ip(app_config)
    print('External IP found: {}'.format(my_ip))
    for host, host_config in app_config.hosts:
        print('Configuring record "{}..."'.format(host))
        dns_record = GandiDNSRecord(
            api_url=app_config.dns_api_url,
            config=host_config
        )
        dns_record.update_record_ip(ip_address=my_ip, record_type='A')
        print('Record "{}" configured to IP {}!'.format(host, my_ip))


if __name__ == '__main__':
    main()
